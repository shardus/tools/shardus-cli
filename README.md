# shardus

The Shardus command line tool.

Check out the [docs](https://shardus.gitlab.io/docs/developer/tools/shardus-cli-tool/) for details.

---

<br>
<div align="center">
  <a href="https://shardus.com/" target="_blank">
    <img src="https://raw.githubusercontent.com/Shardus/shardus.github.io/master/assets/img/shardus_logo_256.png" width="100">
  </a>
</div>